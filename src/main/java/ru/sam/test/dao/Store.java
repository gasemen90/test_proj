package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import java.util.List;

import static java.util.Arrays.asList;

public class Store extends TableImpl<StoreRecord> {

    public static final Store STORE = new Store();

    public final TableField<StoreRecord, Long>   STORE_ID     = createField("STORE_ID",           SQLDataType.BIGINT,   this);
//    public final TableField<StoresRecord, String> STORE_NUMBER = createField("store_number", SQLDataType.NCHAR, this);
    public final TableField<StoreRecord, String> DESCR        = createField("DESCR",        SQLDataType.VARCHAR, this);
    public final TableField<StoreRecord, String> ADDRESS      = createField("ADDRESS",      SQLDataType.VARCHAR, this);
    public final TableField<StoreRecord, Long>   ORG_ID       = createField("ORG_ID",       SQLDataType.BIGINT, this);

    public Store() {
        this("STORE", null);
    }

    public Store(String alias) {
        this(alias, STORE);
    }

    private Store(String alias, Table<StoreRecord> aliased) {
        this(alias, aliased, null);
    }

    private Store(String alias, Table<StoreRecord> aliased, Field<?>[] parameters) {
        super(alias, DefaultSchema.DEFAULT_SCHEMA, aliased, parameters);
    }

    @Override
    public Class<StoreRecord> getRecordType() {
        return StoreRecord.class;
    }

    @Override
    public UniqueKey<StoreRecord> getPrimaryKey() {
        return Keys.PK_STORES;
    }

    @Override
    public List<UniqueKey<StoreRecord>> getKeys() {
        return asList(Keys.PK_STORES);
    }

    @Override
    public List<ForeignKey<StoreRecord, ?>> getReferences() {
        return asList(Keys.FK_STORES_ORG);
    }

    @Override
    public Store as(String alias) {
        return new Store(alias, this);
    }

}
