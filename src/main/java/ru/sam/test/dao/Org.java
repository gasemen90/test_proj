package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import java.util.List;

import static java.util.Arrays.asList;

public class Org extends TableImpl<OrgRecord> {

    public static final Org ORG = new Org();

    public final TableField<OrgRecord, Long> ORG_ID = createField("ORG_ID",          SQLDataType.BIGINT,   this);
    public final TableField<OrgRecord, String>    NAME        = createField("NAME",        SQLDataType.VARCHAR, this);
//    public final TableField<OrgsRecord, Timestamp> CR          = createField("cr",          SQLDataType.TIMESTAMP, this);
//    public final TableField<OrgsRecord, Timestamp> LU          = createField("lu",          SQLDataType.TIMESTAMP, this);
//    public final TableField<OrgsRecord, Timestamp> EN          = createField("en",          SQLDataType.TIMESTAMP, this);
//    public final TableField<OrgsRecord, Date>      ACTIVE_FROM = createField("active_from", SQLDataType.DATE, this);
//    public final TableField<OrgsRecord, Date>      ACTIVE_TILL = createField("active_till", SQLDataType.DATE, this);

    public Org() {
        this("ORG", null);
    }

    public Org(String alias) {
        this(alias, ORG);
    }

    private Org(String alias, Table<OrgRecord> aliased) {
        this(alias, aliased, null);
    }

    private Org(String alias, Table<OrgRecord> aliased, Field<?>[] parameters) {
        super(alias, DefaultSchema.DEFAULT_SCHEMA, aliased, parameters);
    }

    @Override
    public Class<OrgRecord> getRecordType() {
        return OrgRecord.class;
    }

    @Override
    public UniqueKey<OrgRecord> getPrimaryKey() {
        return Keys.PK_ORGS;
    }

    @Override
    public List<UniqueKey<OrgRecord>> getKeys() {
        return asList(Keys.PK_ORGS);
    }

    @Override
    public Org as(String alias) {
        return new Org(alias, this);
    }

}
