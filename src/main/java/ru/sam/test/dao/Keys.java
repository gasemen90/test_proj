package ru.sam.test.dao;

import org.jooq.ForeignKey;
import org.jooq.UniqueKey;
import org.jooq.impl.AbstractKeys;

import static ru.sam.test.dao.Mhr.MHR;

public class Keys {

    public static final UniqueKey<OrgRecord>       PK_ORGS   = UniqueKeys.PK_ORGS;
    public static final UniqueKey<StoreRecord>     PK_STORES = UniqueKeys.PK_STORES;
    public static final UniqueKey<OperationRecord> PK_OPERATIONS = UniqueKeys.PK_OPERATIONS;
    public static final UniqueKey<MhrRecord>        PK_MHR        = UniqueKeys.PK_MHR;

    public static final ForeignKey<StoreRecord, OrgRecord>     FK_STORES_ORG = ForeignKeys0.FK_STORES_ORG;
    public static final ForeignKey<MhrRecord, OperationRecord>  FK_MHR_OPERATION = ForeignKeys0.FK_MHR_OPERATION;
    public static final ForeignKey<MhrRecord, OrgRecord>        FK_MHR_ORG     = ForeignKeys0.FK_MHR_ORG;
    public static final ForeignKey<MhrRecord, StoreRecord>      FK_MHR_STORE           = ForeignKeys0.FK_MHR_STORE;

    private static class UniqueKeys extends AbstractKeys {
        public static final UniqueKey<OrgRecord>       PK_ORGS       = createUniqueKey(Org.ORG, Org.ORG.ORG_ID);
        public static final UniqueKey<StoreRecord>     PK_STORES     = createUniqueKey(Store.STORE, Store.STORE.STORE_ID);
        public static final UniqueKey<OperationRecord> PK_OPERATIONS = createUniqueKey(Operation.OPERATION, Operation.OPERATION.OPERATION_ID);
        public static final UniqueKey<MhrRecord>        PK_MHR        = createUniqueKey(MHR, MHR.MHR_ID);
    }

    private static class ForeignKeys0 extends AbstractKeys {
        public static final ForeignKey<StoreRecord, OrgRecord> FK_STORES_ORG          = createForeignKey(PK_ORGS, Store.STORE, Store.STORE.ORG_ID);
        public static final ForeignKey<MhrRecord, OperationRecord> FK_MHR_OPERATION   = createForeignKey(PK_OPERATIONS, MHR, MHR.OPERATION_ID);
        public static final ForeignKey<MhrRecord, OrgRecord> FK_MHR_ORG               = createForeignKey(PK_ORGS, MHR, MHR.ORG_ID);
        public static final ForeignKey<MhrRecord, StoreRecord> FK_MHR_STORE           = createForeignKey(PK_STORES, MHR, MHR.STORE_ID);
    }
}
