package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StoreRecord extends UpdatableRecordImpl<StoreRecord> implements Record4<Long, String, String, Long> {
    protected static final Logger log = LoggerFactory.getLogger(StoreRecord.class);

    public StoreRecord() {
        super(Store.STORE);
    }

    public StoreRecord(Long id, String descr, String address, Long orgId) {
        super(Store.STORE);
        setValue(0, id);
        setValue(1, descr);
        setValue(2, address);
        setValue(3, orgId);
    }

    @Override
    public Record1<Long> key() {
        return Utils.cast(super.key());
    }

    @Override
    public Row4<Long, String, String, Long> fieldsRow() {
        return Utils.cast(super.fieldsRow());
    }

    @Override
    public Row4<Long, String, String, Long> valuesRow() {
        return Utils.cast(super.valuesRow());
    }

    @Override
    public Field<Long> field1() {
        return Store.STORE.STORE_ID;
    }

    @Override
    public Field<String> field2() {
        return Store.STORE.DESCR;
    }

    @Override
    public Field<String> field3() {
        return Store.STORE.ADDRESS;
    }

    @Override
    public Field<Long> field4() {
        return Store.STORE.ORG_ID;
    }


    @Override
    public Long value1() {
        return Utils.cast(getValue(0));
    }

    @Override
    public String value2() {
        return Utils.cast(getValue(1));
    }

    @Override
    public String value3() {
        return Utils.cast(getValue(2));
    }

    @Override
    public Long value4() {
        return Utils.cast(getValue(3));
    }

    @Override
    public StoreRecord value1(Long value) {
        setValue(0, value);
        return this;
    }

    @Override
    public StoreRecord value2(String value) {
        setValue(1, value);
        return this;
    }

    @Override
    public StoreRecord value3(String value) {
        setValue(2, value);
        return this;
    }

    @Override
    public StoreRecord value4(Long value) {
        setValue(3, value);
        return this;
    }

    @Override
    public StoreRecord values(Long id, String descr, String address, Long orgId) {
        value1(id);
        value2(descr);
        value3(address);
        value4(orgId);
        return this;
    }
}
