package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import static java.util.Arrays.asList;

public class Mhr extends TableImpl<MhrRecord> {

    public static final Mhr MHR = new Mhr();

    public final TableField<MhrRecord, Long>       MHR_ID       = createField("MHR_ID",           SQLDataType.BIGINT, this);
    public final TableField<MhrRecord, String>     DESC         = createField("DESCR",     SQLDataType.VARCHAR, this);
    public final TableField<MhrRecord, Timestamp>  PERIOD       = createField("PERIOD",   SQLDataType.TIMESTAMP, this);
    public final TableField<MhrRecord, BigDecimal> QTY          = createField("QTY",      SQLDataType.NUMERIC, this);
    public final TableField<MhrRecord, Long>       OPERATION_ID = createField("OPERATION_ID", SQLDataType.BIGINT, this);
    public final TableField<MhrRecord, Long>       ORG_ID       = createField("ORG_ID",       SQLDataType.BIGINT, this);
    public final TableField<MhrRecord, Long>       STORE_ID     = createField("STORE_ID",     SQLDataType.BIGINT, this);

    public Mhr() {
        this("MHR", null);
    }

    public Mhr(String alias) {
        this(alias, MHR);
    }

    private Mhr(String alias, Table<MhrRecord> aliased) {
        this(alias, aliased, null);
    }

    private Mhr(String alias, Table<MhrRecord> aliased, Field<?>[] parameters) {
        super(alias, DefaultSchema.DEFAULT_SCHEMA, aliased, parameters);
    }

    @Override
    public Class<MhrRecord> getRecordType() {
        return MhrRecord.class;
    }

    @Override
    public UniqueKey<MhrRecord> getPrimaryKey() {
        return Keys.PK_MHR;
    }

    @Override
    public List<UniqueKey<MhrRecord>> getKeys() {
        return asList(Keys.PK_MHR);
    }

    @Override
    public List<ForeignKey<MhrRecord, ?>> getReferences() {
        return asList(Keys.FK_MHR_OPERATION, Keys.FK_MHR_ORG, Keys.FK_MHR_STORE);
    }

    @Override
    public Mhr as(String alias) {
        return new Mhr(alias, this);
    }
}
