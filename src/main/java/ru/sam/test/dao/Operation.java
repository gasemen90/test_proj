package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;

import java.util.List;

import static java.util.Arrays.asList;

public class Operation extends TableImpl<OperationRecord> {

    public static final Operation OPERATION = new Operation();

    public final TableField<OperationRecord, Long> OPERATION_ID = createField("OPERATION_ID",     SQLDataType.BIGINT,  this);
    public final TableField<OperationRecord, String>  DESCR  = createField("DESCR",  SQLDataType.VARCHAR, this);
//    public final TableField<OperationsRecord, Long>    ORG_ID = createField("org_id", SQLDataType.BIGINT,  this);
//    public final TableField<OperationsRecord, Boolean> ACTIVE = createField("active", SQLDataType.BOOLEAN, this);
//    public final TableField<OperationsRecord, String>  NOTE   = createField("note",   SQLDataType.VARCHAR, this);


    public Operation() {
        this("OPERATION", null);
    }

    public Operation(String alias) {
        this(alias, OPERATION);
    }

    private Operation(String alias, Table<OperationRecord> aliased) {
        this(alias, aliased, null);
    }

    private Operation(String alias, Table<OperationRecord> aliased, Field<?>[] parameters) {
        super(alias, DefaultSchema.DEFAULT_SCHEMA, aliased, parameters);
    }

    @Override
    public Class<OperationRecord> getRecordType() {
        return OperationRecord.class;
    }

    @Override
    public UniqueKey<OperationRecord> getPrimaryKey() {
        return Keys.PK_OPERATIONS;
    }

    @Override
    public List<UniqueKey<OperationRecord>> getKeys() {
        return asList(Keys.PK_OPERATIONS);
    }

    @Override
    public Operation as(String alias) {
        return new Operation(alias, this);
    }


}
