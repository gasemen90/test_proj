package ru.sam.test.dao;

import org.jooq.Table;
import org.jooq.impl.SchemaImpl;

import java.util.ArrayList;
import java.util.List;

import static ru.sam.test.dao.Mhr.MHR;
import static ru.sam.test.dao.Operation.OPERATION;
import static java.util.Arrays.asList;

public class DefaultSchema extends SchemaImpl {

    public static final DefaultSchema DEFAULT_SCHEMA = new DefaultSchema();

    private DefaultSchema() {
        super("");
    }

    @Override
    public final List<Table<?>> getTables() {
        return new ArrayList<>(asList(
                Org.ORG,
                Store.STORE,
                OPERATION,
                MHR
        ));
    }

}
