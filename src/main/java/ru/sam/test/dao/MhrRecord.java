package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;

import java.math.BigDecimal;
import java.sql.Timestamp;

import static ru.sam.test.dao.Mhr.MHR;
import static ru.sam.test.dao.Utils.cast;

public class MhrRecord extends UpdatableRecordImpl<MhrRecord> implements Record7<Long, String, Timestamp, BigDecimal, Long, Long, Long> {

    public MhrRecord() {
        super(MHR);
    }

    public MhrRecord(Long id, String desc, Timestamp period, Double qty, Long operationId, Long orgId, Long storeId) {
        super(MHR);
        setValue(0, id);
        setValue(1, desc);
        setValue(2, period);
        setValue(3, qty);
        setValue(4, operationId);
        setValue(5, orgId);
        setValue(6, storeId);
    }

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    @Override
    public Row7<Long, String, Timestamp, BigDecimal, Long, Long, Long> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<Long, String, Timestamp, BigDecimal, Long, Long, Long> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<Long> field1() {
        return MHR.MHR_ID;
    }

    @Override
    public Field<String> field2() {
        return MHR.DESC;
    }

    @Override
    public Field<Timestamp> field3() {
        return MHR.PERIOD;
    }

    @Override
    public Field<BigDecimal> field4() {
        return MHR.QTY;
    }

    @Override
    public Field<Long> field5() {
        return MHR.OPERATION_ID;
    }

    @Override
    public Field<Long> field6() {
        return MHR.ORG_ID;
    }

    @Override
    public Field<Long> field7() {
        return MHR.STORE_ID;
    }

    @Override
    public Long value1() {
        return cast(getValue(0));
    }

    @Override
    public String value2() {
        return cast(getValue(1));
    }

    @Override
    public Timestamp value3() {
        return cast(getValue(2));
    }

    @Override
    public BigDecimal value4() {
        return cast(getValue(3));
    }

    @Override
    public Long value5() {
        return cast(getValue(4));
    }

    @Override
    public Long value6() {
        return cast(getValue(5));
    }

    @Override
    public Long value7() {
        return cast(getValue(6));
    }

    @Override
    public MhrRecord value1(Long value) {
        setValue(0, value);
        return this;
    }

    @Override
    public MhrRecord value2(String value) {
        setValue(1, value);
        return this;
    }

    @Override
    public MhrRecord value3(Timestamp value) {
        setValue(2, value);
        return this;
    }

    @Override
    public MhrRecord value4(BigDecimal value) {
        setValue(3, value);
        return this;
    }

    @Override
    public MhrRecord value5(Long value) {
        setValue(4, value);
        return this;
    }

    @Override
    public MhrRecord value6(Long value) {
        setValue(5, value);
        return this;
    }

    @Override
    public MhrRecord value7(Long value) {
        setValue(6, value);
        return this;
    }

    @Override
    public MhrRecord values(Long id, String desc, Timestamp period, BigDecimal qty, Long operationId, Long orgId, Long storeId) {
        value1(id);
        value2(desc);
        value3(period);
        value4(qty);
        value5(operationId);
        value6(orgId);
        value7(storeId);
        return this;
    }

}
