package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;

import java.sql.Date;
import java.sql.Timestamp;

public class OrgRecord extends UpdatableRecordImpl<OrgRecord> implements Record2<Long, String> {

    public OrgRecord() {
        super(Org.ORG);
    }

    public OrgRecord(Long id, String name, Timestamp cr, Timestamp lu, Timestamp en, Date activeFrom, Date activeTill) {
        super(Org.ORG);
        setValue(0, id);
        setValue(1, name);
        setValue(2, cr);
        setValue(3, lu);
        setValue(4, en);
        setValue(5, activeFrom);
        setValue(6, activeTill);
    }

    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    @Override
    public Row2<Long, String> fieldsRow() {
        return Utils.cast(super.fieldsRow());
    }

    @Override
    public Row2<Long, String> valuesRow() {
        return Utils.cast(super.valuesRow());
    }

    @Override
    public Field<Long> field1() {
        return Org.ORG.ORG_ID;
    }

    @Override
    public Field<String> field2() {
        return Org.ORG.NAME;
    }

    @Override
    public Long value1() {
        return Utils.cast(getValue(0));
    }

    @Override
    public String value2() {
        return Utils.cast(getValue(1));
    }

    @Override
    public OrgRecord value1(Long value) {
        setValue(0, value);
        return this;
    }

    @Override
    public OrgRecord value2(String value) {
        setValue(1, value);
        return this;
    }

    @Override
    public OrgRecord values(Long id, String name) {
        value1(id);
        value2(name);
        return this;
    }

    public Long getId() {
        return value1();
    }

    public void setId(Long id) {
        value1(id);
    }

    public String getName() {
        return value2();
    }

    public void setName(String name) {
        value2(name);
    }

}
