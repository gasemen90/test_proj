package ru.sam.test.dao;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ru.sam.test.dao.Operation.OPERATION;
import static ru.sam.test.dao.Utils.cast;

public class OperationRecord extends UpdatableRecordImpl<OperationRecord> implements Record2<Long, String> {
    protected static final Logger log = LoggerFactory.getLogger(OperationRecord.class);

    public OperationRecord() {
        super(OPERATION);
    }

    public OperationRecord(Long id, String descr) {
        super(OPERATION);
        setValue(0, id);
        setValue(1, descr);
    }

    @Override
    public Record1<Long> key() {
        return cast(super.key());
    }

    @Override
    public Row2<Long, String> fieldsRow() {
        return cast(super.fieldsRow());
    }

    @Override
    public Row2<Long, String> valuesRow() {
        return cast(super.valuesRow());
    }

    @Override
    public Field<Long> field1() {
        return OPERATION.OPERATION_ID;
    }

    @Override
    public Field<String> field2() {
        return OPERATION.DESCR;
    }

    @Override
    public Long value1() {
        return cast(getValue(0));
    }

    @Override
    public String value2() {
        return cast(getValue(1));
    }

    @Override
    public OperationRecord value1(Long value) {
        setValue(0, value);
        return this;
    }

    @Override
    public OperationRecord value2(String value) {
        setValue(1, value);
        return this;
    }

    @Override
    public OperationRecord values(Long id, String descr) {
        value1(id);
        value2(descr);
        return this;
    }

}
