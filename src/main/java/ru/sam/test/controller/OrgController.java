package ru.sam.test.controller;

import ru.sam.test.model.OrgPayload;
import ru.sam.test.service.OrgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = "application/json;charset=UTF-8")
public class OrgController {
    protected static final Logger log = LoggerFactory.getLogger(OrgController.class);

    @Autowired
    private OrgService orgService;

    @RequestMapping(value = "/orgs", method = RequestMethod.GET)
    public ResponseEntity<List<OrgPayload>> orgs() {
        final List<OrgPayload> result = orgService.getOrgs();
        if (result.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return ResponseEntity.ok(result);
        }
    }
}
