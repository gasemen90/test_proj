package ru.sam.test.controller;

import ru.sam.test.model.ReportPayload;
import ru.sam.test.service.ReportService;
import com.fasterxml.jackson.annotation.JsonProperty;
import net.sf.jasperreports.engine.JRException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;

// report?storeId=73514&date=01.10.2015

@RestController
@RequestMapping(value = "/api", produces = "application/json;charset=UTF-8")
public class ReportController {
    protected static final Logger log = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public ResponseEntity<ReportPayload> report(@RequestBody ReportRequestPayload requestPayload) {
        final ReportPayload result = reportService.getReportData(requestPayload.getStoreId(), requestPayload.getDate());

        if (result.equals(ReportPayload.NULL)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return ResponseEntity.ok(result);
        }
    }

    @RequestMapping(value = "/report/xls", method = RequestMethod.POST)
    public ResponseEntity<FileNameResponsePayload> reportXls(@RequestBody ReportRequestPayload requestPayload) {
        try {
            final String fileName = reportService.getReportForXlsExport(requestPayload.getStoreId(), requestPayload.getDate());
            if (fileName.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            } else {
                return ResponseEntity.ok(new FileNameResponsePayload(fileName));
            }
        } catch (IOException | JRException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    public static class ReportRequestPayload {
        @JsonProperty
        private Long storeId;
        @JsonProperty
        private Date date;

        public Long getStoreId() {
            return storeId;
        }

        public void setStoreId(Long storeId) {
            this.storeId = storeId;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }

    public static class FileNameResponsePayload {
        @JsonProperty
        private String fileName;

        public FileNameResponsePayload(String fileName) {
            this.fileName = fileName;
        }

        public String getFileName() {
            return fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }
    }

}
