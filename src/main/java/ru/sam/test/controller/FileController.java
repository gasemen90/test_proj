package ru.sam.test.controller;

import ru.sam.test.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping(value = "/api/files")
public class FileController {
    protected static final Logger log = LoggerFactory.getLogger(FileController.class);

    private static final HttpHeaders HEADERS = new HttpHeaders();
    static {
        HEADERS.add("Cache-Control", "no-cache, no-store, must-revalidate");
        HEADERS.add("Pragma", "no-cache");
        HEADERS.add("Expires", "0");
        HEADERS.add("Content-Disposition", "attachment");
    }

    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/xls/{fileName:.+}", method = RequestMethod.GET, produces = "application/vnd.ms-excel")
    public ResponseEntity<InputStreamResource> getXls(@PathVariable String fileName) {
        log.debug("FileName: {}", fileName);
        try {
            final InputStream file = fileService.getFile(fileName);
            if (file == null) {
                log.warn("File {} is not found", fileName);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
            }
            return ResponseEntity
                    .ok()
                    .headers(HEADERS)
                    .contentLength(file.available())
                    .body(new InputStreamResource(file));
        } catch (IOException ioe) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }
}
