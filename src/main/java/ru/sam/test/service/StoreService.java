package ru.sam.test.service;

import ru.sam.test.model.StorePayload;
import org.jooq.DSLContext;
import org.jooq.Record3;
import org.jooq.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sam.test.dao.Store;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class StoreService {
    protected static final Logger log = LoggerFactory.getLogger(StoreService.class);

    @Autowired
    private DSLContext dsl;

    public List<StorePayload> getStores(Long orgId) {
        final Result<Record3<Long, String, String>> result = dsl
                .select(Store.STORE.STORE_ID, Store.STORE.DESCR, Store.STORE.ADDRESS)
                .from(Store.STORE)
                .where(Store.STORE.ORG_ID.eq(orgId))
                .orderBy(Store.STORE.DESCR, Store.STORE.ADDRESS)
                .fetch();
        if (result == null) {
            return Collections.emptyList();
        }
        final List<StorePayload> payloads = new ArrayList<>();
        for (Record3<Long, String, String> row : result) {
            payloads.add(new StorePayload(row.value1(), row.value2(), row.value3()));
        }
        return payloads;
    }
}
