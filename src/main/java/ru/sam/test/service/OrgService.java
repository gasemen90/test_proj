package ru.sam.test.service;

import ru.sam.test.model.OrgPayload;
import org.jooq.DSLContext;
import org.jooq.Record2;
import org.jooq.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sam.test.dao.Org;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class OrgService {
    protected static final Logger log = LoggerFactory.getLogger(OrgService.class);

    @Autowired
    private DSLContext dsl;

    public List<OrgPayload> getOrgs() {
        final Result<Record2<Long, String>> result = dsl
                .select(Org.ORG.ORG_ID, Org.ORG.NAME)
                .from(Org.ORG)
                .orderBy(Org.ORG.NAME)
                .fetch();
        if (result == null) {
            return Collections.emptyList();
        }
        final List<OrgPayload> payloads = new ArrayList<>();
        for (Record2<Long, String> row : result) {
            payloads.add(new OrgPayload(row.value1(), row.value2()));
        }
        return payloads;
    }
}
