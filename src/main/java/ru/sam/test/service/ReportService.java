package ru.sam.test.service;

import ru.sam.test.model.OperationPayload;
import ru.sam.test.model.RangePayload;
import ru.sam.test.model.ReportPayload;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.DatePart;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Result;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sam.test.dao.Mhr;
import ru.sam.test.dao.Operation;
import ru.sam.test.dao.Org;
import ru.sam.test.dao.Store;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

@Service
@Transactional
public class ReportService {
    protected static final Logger log = LoggerFactory.getLogger(ReportService.class);
    private static final String JASPER_FILE = "report_template.jasper";
    private static final String REPORT_TEMPLATE_PATH = getReportTemplatePath();

    @Autowired
    private DSLContext dsl;

    @Autowired
    private FileService fileService;

    public ReportPayload getReportData(Long storeId, java.util.Date date) {
        final Date sqlDate = new Date(date.getTime());

        final Condition condition = Mhr.MHR.STORE_ID.eq(storeId)
                .and(DSL.trunc(Mhr.MHR.PERIOD, DatePart.DAY).cast(Date.class).eq(sqlDate));

        final Record2<Integer, Integer> limits = dsl
                .select(DSL.min(DSL.extract(Mhr.MHR.PERIOD, DatePart.HOUR)), DSL.max(DSL.extract(Mhr.MHR.PERIOD, DatePart.HOUR)))
                .from(Mhr.MHR)
                .where(Mhr.MHR.QTY.ne(BigDecimal.ZERO))
                .and(condition)
                .fetchOne();
        if (asList(limits.value1(), limits.value2()).contains(null)) {
            return ReportPayload.NULL;
        }
        final Map<Long, Result<Record4<Long, String, Integer, BigDecimal>>> result = dsl
                .select(Mhr.MHR.OPERATION_ID, Operation.OPERATION.DESCR, DSL.extract(Mhr.MHR.PERIOD, DatePart.HOUR), Mhr.MHR.QTY.sum())
                .from(Mhr.MHR)
                .join(Operation.OPERATION).on(Mhr.MHR.OPERATION_ID.eq(Operation.OPERATION.OPERATION_ID))
                .where(condition)
                .groupBy(Mhr.MHR.OPERATION_ID, Operation.OPERATION.DESCR, Mhr.MHR.PERIOD)
                .orderBy(Operation.OPERATION.DESCR, Mhr.MHR.PERIOD)
                .fetchGroups(Mhr.MHR.OPERATION_ID);
        if (result == null) {
            return ReportPayload.NULL;
        }
        final ReportPayload reportPayload = new ReportPayload();
        reportPayload.setRange(new RangePayload(limits.value1(), limits.value2()));
        final List<ReportPayload.Payload> payloads = new ArrayList<>();
        for (Long operationId : result.keySet()) {
            final Result<Record4<Long, String, Integer, BigDecimal>> line = result.get(operationId);

            final ReportPayload.Payload payload = new ReportPayload.Payload();
            payload.setOperation(new OperationPayload(operationId, line.getValue(0, Operation.OPERATION.DESCR)));
            final Map<Integer, BigDecimal> rs = new HashMap<>();
            for (Record4<Long, String, Integer, BigDecimal> cell : line) {
                rs.put(cell.value3(), cell.value4().setScale(0, RoundingMode.CEILING));
            }
            payload.setResults(rs);
            payloads.add(payload);
        }
        reportPayload.setPayload(payloads);
        return reportPayload;
    }

    public String getReportForXlsExport(Long storeId, java.util.Date date) throws JRException, IOException {
        final Record2<String, String> titleInfo = dsl
                .select(Org.ORG.NAME, Store.STORE.DESCR.concat(" ").concat(Store.STORE.ADDRESS))
                .from(Store.STORE)
                .join(Org.ORG).on(Store.STORE.ORG_ID.eq(Org.ORG.ORG_ID))
                .where(Store.STORE.STORE_ID.eq(storeId))
                .fetchOne();

        if (titleInfo == null) {
            return "";
        }

        final Date sqlDate = new Date(date.getTime());
        final Result<Record3<String, Integer, BigDecimal>> result = dsl
                .select(Operation.OPERATION.DESCR.as("descr"), DSL.extract(Mhr.MHR.PERIOD, DatePart.HOUR).as("hour"), Mhr.MHR.QTY.sum().as("qty"))
                .from(Mhr.MHR)
                .join(Operation.OPERATION).on(Mhr.MHR.OPERATION_ID.eq(Operation.OPERATION.OPERATION_ID))
                .where(Mhr.MHR.STORE_ID.eq(storeId))
                .and(DSL.trunc(Mhr.MHR.PERIOD, DatePart.DAY).cast(Date.class).eq(sqlDate))
                .groupBy(Mhr.MHR.OPERATION_ID, Operation.OPERATION.DESCR, Mhr.MHR.PERIOD)
                .orderBy(Mhr.MHR.OPERATION_ID, Operation.OPERATION.DESCR, Mhr.MHR.PERIOD)
                .fetch();

        if (result.isEmpty()) {
            return "";
        }

        final Map<String, Object> params = new HashMap<>();
        params.put("ReportTitle", "Отчет");
        params.put("Organization", titleInfo.value1());
        params.put("Object", titleInfo.value2());
        params.put("Date", date);

        final String fileName = getReportFileName(titleInfo.value1(), titleInfo.value2(), date);

        final Path destFile = fileService.createTempPath(fileName, ".jsreport");
        final String destFileName = destFile.toAbsolutePath().toString();

        JasperFillManager.fillReportToFile(REPORT_TEMPLATE_PATH, destFileName, params, new JRResultSetDataSource(result.intoResultSet()));

        final Path reportFile = fileService.createTempPath(fileName, ".xls");
        final String reportFileName = reportFile.toAbsolutePath().toString();

        final JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(destFileName));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportFileName));
        exporter.exportReport();

        Files.delete(destFile);

        log.debug("Path: {}", reportFileName);
        return reportFile.getFileName().toString();
    }

    private static String getReportTemplatePath() {
        try {
            return new ClassPathResource(JASPER_FILE).getURI().getPath();
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
            throw new RuntimeException(ioe);
        }
    }

    private static String getReportFileName(String orgName, String storeName, java.util.Date date) {
        return String.format("Отчет_%s_%s_от_%s_",
                orgName, storeName, new SimpleDateFormat("dd_MM_yyyy").format(date)).replace(' ', '_');
    }

}
