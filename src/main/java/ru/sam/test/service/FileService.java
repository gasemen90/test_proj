package ru.sam.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class FileService {
    protected static final Logger log = LoggerFactory.getLogger(FileService.class);

    public InputStream getFile(String fileName) {
        final Path path = TempDirSingleton.getInstance().reportsDir.resolve(fileName);
        if (Files.exists(path)) {
            try {
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Files.copy(path, baos);
                Files.delete(path);
                return new ByteArrayInputStream(baos.toByteArray());
            } catch (IOException ioe) {
                log.error(ioe.getMessage(), ioe);
            }
        }
        return null;
    }

    public Path createTempPath(String fileName, String extension) {
        try {
            return Files.createTempFile(TempDirSingleton.getInstance().reportsDir, fileName, extension);
        } catch (IOException ioe) {
            log.error(ioe.getMessage(), ioe);
            throw new RuntimeException(ioe);
        }
    }

    private static class TempDirSingleton {
        private static final String TEMP_DIR = "reports";
        private final Path reportsDir;

        private TempDirSingleton() {
            try {
                reportsDir = Files.createTempDirectory(TEMP_DIR);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        public static TempDirSingleton getInstance() {
            return SingletonHolder.HOLDER_INSTANCE;
        }

        private static class SingletonHolder {
            public static final TempDirSingleton HOLDER_INSTANCE = new TempDirSingleton();
        }
    }
}
