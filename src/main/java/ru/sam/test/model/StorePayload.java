package ru.sam.test.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class StorePayload {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String descr;

    @JsonProperty
    private String address;

    public StorePayload() {
        // empty
    }

    public StorePayload(Long id, String descr, String address) {
        this.id = id;
        this.descr = descr;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
