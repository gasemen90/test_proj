package ru.sam.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OperationPayload {

    @JsonProperty
    private Long id;

    @JsonProperty
    private String descr;

    public OperationPayload() {
        // empty
    }

    public OperationPayload(Long id, String descr) {
        this.id = id;
        this.descr = descr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
