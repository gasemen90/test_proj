package ru.sam.test.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@JsonAutoDetect
public class ReportPayload {

    public static ReportPayload NULL = new ReportPayload() {
        @Override
        public RangePayload getRange() {
            return null;
        }

        @Override
        public void setRange(RangePayload range) {
            // empty
        }

        @Override
        public List<Payload> getPayload() {
            return null;
        }

        @Override
        public void setPayload(List<Payload> payload) {
            // empty
        }
    };

    @JsonProperty
    private RangePayload range;
    @JsonProperty
    private List<Payload> payload;

    public ReportPayload() {
        // empty
    }

    public ReportPayload(RangePayload range, List<Payload> payload) {
        this.range = range;
        this.payload = payload;
    }

    public RangePayload getRange() {
        return range;
    }

    public void setRange(RangePayload range) {
        this.range = range;
    }

    public List<Payload> getPayload() {
        return payload;
    }

    public void setPayload(List<Payload> payload) {
        this.payload = payload;
    }

    public static class Payload {
        @JsonProperty
        private OperationPayload operation;
        @JsonProperty
        private Map<Integer, BigDecimal> results;

        public Payload() {
            // empty
        }

        public Payload(OperationPayload operation, Map<Integer, BigDecimal> results) {
            this.operation = operation;
            this.results = results;
        }

        public OperationPayload getOperation() {
            return operation;
        }

        public void setOperation(OperationPayload operation) {
            this.operation = operation;
        }

        public Map<Integer, BigDecimal> getResults() {
            return results;
        }

        public void setResults(Map<Integer, BigDecimal> results) {
            this.results = results;
        }
    }
}
