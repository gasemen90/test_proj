package ru.sam.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RangePayload {

    @JsonProperty
    private Integer lower;

    @JsonProperty
    private Integer upper;

    public RangePayload() {
        // empty
    }

    public RangePayload(Integer lower, Integer upper) {
        this.lower = lower;
        this.upper = upper;
    }

    public Integer getLower() {
        return lower;
    }

    public void setLower(Integer lower) {
        this.lower = lower;
    }

    public Integer getUpper() {
        return upper;
    }

    public void setUpper(Integer upper) {
        this.upper = upper;
    }
}
