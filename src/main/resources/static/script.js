$(document).ready(function() {

    var templates = {
        optionEl:    _.template($("#option-template").html()),
        reportTable: _.template($("#report-template").html(), { variable: "data"}),
        alert:       _.template($("#alert-template").html(),  { variable: "data"}),
        waitSpinner: _.template($("#wait-spinner-template").html())
    };

    var ALERT_TYPE = Object.freeze({
        success: "alert-success",
        info:    "alert-info",
        warning: "alert-warning",
        danger:  "alert-danger"
    });

    var orgs = $("select[name='orgs']");
    var stores = $("select[name='stores']");
    var date = $("input[name='date']");
    var reportBtn = $("button[name='reportBtn']");
    var exportBtn = $("button[name='exportBtn']");
    var reportTable = $("table[name='reportTable']");
    var alertElem = $("div[name='alert']");

    function formatStr(str, ...args) {
        return str.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    }

    var api = (function() {
        var prefix = "/api";
        return function(path) {
            return prefix + path;
        }
    })();

    var settings = (function() {
        var s = Object.freeze({
            contentType: "application/json;charset=utf-8",
            dataType: "json"
        });
        return function(path, data) {
            return $.extend({ url: api(path), data: JSON.stringify(data || {}) }, s);
        }
    })();

    function showInfo(text, className) {
        alertElem.empty();
        alertElem.append(templates.alert({
            text: text,
            class: className
        }));
    };

    $.get(settings("/orgs")).always(function(data, textStatus, jqXHR) {
        if (jqXHR.status == 200) {
            orgs.empty();
            if (!_.isEmpty(data)) {
                for (var i = 0; i < data.length; i++) {
                    orgs.append(templates.optionEl(data[i]));
                }
                orgs.selectpicker("refresh");
            }
        } else if (data.status == 404) {
            showInfo("Данные об организациях отсутствуют", ALERT_TYPE.warning);
        }
    });

    (function() {
        function formatStoreName(descr, address) {
            var res = descr;
            if (!_.isEmpty(address)) {
                res += formatStr(" ({0})", address);
            }
            return res;
        }
        orgs.on("refreshed.bs.select changed.bs.select", function() {
            $.get(settings(formatStr("/stores?orgId={0}", this.value))).always(function(data, textStatus, jqXHR) {
                if (jqXHR.status == 200) {
                    var stores = $("select[name='stores']");
                    stores.empty();
                    if (!_.isEmpty(data)) {
                        for (var i = 0; i < data.length; i++) {
                            var opt = { id: data[i].id, name: formatStoreName(data[i].descr, data[i].address) };
                            stores.append(templates.optionEl(opt));
                        }
                        stores.selectpicker("refresh");
                    }
                } else if (data.status == 404) {
                    showInfo("Данные об объектах отсутствуют", ALERT_TYPE.warning);
                }
            });
        });
    })();

    reportBtn.click(function() {
        if (!stores.val() || !stores.val().length) {
            showInfo("Не выбран объект", ALERT_TYPE.danger);
            return;
        }
        if (!date.val() || !date.val().length) {
            showInfo("Не указана дата отчета", ALERT_TYPE.danger);
            return;
        }
        var waitSpinner = $(templates.waitSpinner());
        reportBtn.append(waitSpinner);
        var data = {
            storeId: stores.val(),
            date: date.val(),
        };
        alertElem.empty();
        reportTable.empty();
        $.post(settings("/report", data)).always(function(data, textStatus, jqXHR) {
            waitSpinner.remove();
            if (jqXHR.status == 200) {
                var range = {};
                for (var i = data.range.lower; i <= data.range.upper; i++) {
                    range[i] = 0;
                };
                range = Object.freeze(range);
                for (var i = 0; i < data.payload.length; i++) {
                    data.payload[i].results = $.extend({}, range, data.payload[i].results);
                };
                reportTable.append(templates.reportTable(data));
            } else if (data.status == 404) {
                showInfo("Данные отсутствуют", ALERT_TYPE.warning);
            } else {
                showInfo("При формировании отчета произошла ошибка", ALERT_TYPE.danger);
            }
        });
    });

    exportBtn.click(function() {
        if (!stores.val() || !stores.val().length) {
            showInfo("Не выбран объект", ALERT_TYPE.danger);
            return;
        }
        if (!date.val() || !date.val().length) {
            showInfo("Не указана дата отчета", ALERT_TYPE.danger);
            return;
        }
        var waitSpinner = $(templates.waitSpinner());
        exportBtn.append(waitSpinner);
        var data = {
            storeId: stores.val(),
            date: date.val(),
        };
        $.post(settings("/report/xls", data)).always(function(data, textStatus, jqXHR) {
            waitSpinner.remove();
            if (jqXHR.status == 200 && data.fileName) {
                window.location.assign(api("/files/xls/" + data.fileName));
            } else if (data.status == 404) {
                showInfo("Данные отсутствуют", ALERT_TYPE.warning);
            } else {
                showInfo("При формировании отчета произошла ошибка", ALERT_TYPE.danger);
            }
        });
    });
});